/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const assert = require('assert')
const { DateTime } = require('luxon')

const occurTwice = Symbol('Occur Twice')

class Plan {
  constructor (sched, ev, options) {
    this._targets = this._parseSched(sched)
    this.sched = sched
    this.ev = ev
    this._opt = this._normalizeOptions(options)
  }

  /**
   * #snip "next docblock"
   * Get the first DateTime from `t` onwards at which an event should occur.
   *
   *  - `t`   *luxon.DateTime* the time from when to start looking for an
   *    occurence.
   *  - `max` *luxon.DateTime* the time at which to stop looking for an
   *    occurence.
   *  - returns *luxon.DateTime* the first time between `t` and `max`
   *    (inclusive) when an event should occur, or `null` if there are no
   *    occurences scheduled within that timeframe.
   * #snip
   **/
  next (t, max) {
    // We get the time of the next occurence by advancing each of the time
    // components (minute, hour, day, and month) to their next target. We may
    // need to do a few passes of this: sometimes advancing to the target month
    // brings us off the target day, and vice versa. We just keep advancing
    // by the smallest possible increment until all time components are on
    // target. Then we have the time of the next occurence.
    //
    // We need to keep track of some state for handling Daylight Saving Time
    // (DST). That's what `targetHour` is for. We also need to know whether
    // advancing to the target day took us off the target month; that's what
    // `monthChanged` is for.
    //
    // Luxon DateTimes are immutable. Instead of modifying `t` directly, we
    // reuse the variable name throughout this function. Every time, it gets
    // assigned a fresh instance of modified DateTimes.
    let targetHour, monthChanged

    while (t <= max) {
      (t = this._advanceToTargetMinute(t));
      ({ t, targetHour } = this._advanceToTargetHour(t));
      (t = this._advanceToTargetMonth(t));
      ({ t, monthChanged } = this._advanceToTargetDay(t))
      if (monthChanged) {
        continue
      }
      t = this._handleDST(t, targetHour)
      if (t > max) {
        break
      }

      return t
    }

    return null
  }

  _advanceToTargetMinute (t) {
    const targetMinute = this._getNextTarget(t.minute, this._targets[0])
    if (targetMinute !== null) {
      const inc = this._getIncrement(t.minute, targetMinute, 0, 59)
      t = t.plus({ minutes: inc })
    }

    return t
  }

  _advanceToTargetHour (t) {
    const targetHour = this._getNextTarget(t.hour, this._targets[1])
    if (targetHour !== null && !(this._onTargetDate(t) &&
      this._isFirstHourOfDST(t) && targetHour === t.hour - 1)
    ) {
      const inc = this._getIncrement(t.hour, targetHour, 0, 23)
      t = t.plus({ hours: inc })
      if (t.hour !== targetHour) {
        const adjustInc = this._getIncrement(t.hour, targetHour, 0, 23)
        if (adjustInc === 1) {
          assert.ok(!t.isInDST, 'We are no longer in DST')
          assert.ok(t.minus({ days: 1 }).isInDST, 'But we were in DST yesterday')
          t = t.plus({ hours: 1 })
        } else {
          assert.ok(t.isInDST, 'We are now in DST')
          assert.ok(!t.minus({ days: 1 }).isInDST, 'But we were not in DST yesterday')
          t = t.minus({ hours: 1 })
        }
      }
    }

    return { t, targetHour }
  }

  _advanceToTargetMonth (t) {
    const targetMonth = this._getNextTarget(t.month, this._targets[3])
    if (targetMonth !== null) {
      // Get to the right month in two steps: 1. jump to the month _before_ the
      // target, then 2. jump to the first day of next month.
      const monthInc = this._getIncrement(t.month, targetMonth, 1, 12)
      if (monthInc > 0) {
        t = t.plus({ months: monthInc - 1 })
        t = t.plus({ days: t.daysInMonth - t.day + 1 })
      }
    }

    return t
  }

  _advanceToTargetDay (t) {
    const origMonth = t.month
    const targetDom = this._getNextTarget(t.day, this._targets[2])
    const targetDow = this._getNextTarget(t.weekday, this._targets[4])
    if (targetDom !== null && targetDow !== null) {
      const domInc = this._getIncrement(t.day, targetDom, 1, t.daysInMonth)
      const dowInc = this._getIncrement(t.weekday, targetDow, 1, 7)
      t = t.plus({ days: domInc < dowInc ? domInc : dowInc })
    } else if (targetDom !== null) {
      t = t.plus({ days: this._getIncrement(t.day, targetDom, 1, t.daysInMonth) })
    } else if (targetDow !== null) {
      t = t.plus({ days: this._getIncrement(t.weekday, targetDow, 1, 7) })
    }
    const monthChanged = t.month !== origMonth

    return { t, monthChanged }
  }

  _getNextTarget (current, targets) {
    if (targets === null) {
      return null
    }
    assert(targets.length > 0)

    let next
    for (next of targets) {
      if (next >= current) {
        break
      }
    }

    if (next < current) {
      // current is larger than all targets; this means we need to loop around.
      return targets[0]
    }

    return next
  }

  _getIncrement (current, target, min, max) {
    if (target < current || target > max) {
      return max - current + 1 + target - min
    }

    return target - current
  }

  _onTargetDate (t) {
    return (this._targets[3] === null || t.month === this._getNextTarget(t, this._targets[3])) &&
      ((this._targets[2] === null && this._targets[4] === null) ||
        this._getNextTarget(t, this._targets[2]) === t.day ||
        this._getNextTarget(t, this._targets[4]) === t.weekday)
  }

  _normalizeOptions (options) {
    const ret = {}

    if (typeof options !== 'object') {
      options = {}
    }

    if (['always', 'auto'].includes(options.dstRunSkipped)) {
      ret.dstRunSkipped = options.dstRunSkipped
    }
    if (['first', 'second', 'auto'].includes(options.dstNoRepeat)) {
      ret.dstNoRepeat = options.dstNoRepeat
    }

    return ret
  }

  _parseSched (sched) {
    const lms = sched.trim().split(/\s+/)
    if (lms.length !== 5) {
      throw new Error(`Bad sched '${sched}'. Need 5 elements; have ${lms.length}.`)
    }

    return [
      this._parseMinutes(lms[0]),
      this._parseHours(lms[1]),
      this._parseDoms(lms[2]),
      this._parseMonths(lms[3]),
      this._parseDows(lms[4])
    ]
  }

  _parseMinutes (expr) {
    return this._parseSchedElem(expr, 'minutes', 0, 59, a => {
      if (/^[0-5]?[0-9]$/.test(a)) return parseInt(a, 10)
      else throw new Error('minutes must be between 0-59')
    })
  }

  _parseHours (expr) {
    return this._parseSchedElem(expr, 'hours', 0, 23, a => {
      if (/^\d+$/.test(a) && a < 24) return parseInt(a, 10)
      else throw new Error('hours must be between 0-23')
    })
  }

  _parseDoms (expr) {
    return this._parseSchedElem(expr, 'days of month', 1, 31, a => {
      if (/^\d+$/.test(a) && a >= 1 && a <= 31) return parseInt(a, 10)
      else throw new Error('days of month must be between 1-31')
    })
  }

  _parseMonths (expr) {
    return this._parseSchedElem(expr, 'months', 1, 12, a => {
      const r = parseInt(DateTime.fromFormat(a, 'MMMM').month ||
        DateTime.fromFormat(a, 'MMM').month || a, 10)
      if (Number.isNaN(r) || r < 1 || r > 12) {
        throw new Error('months must be en-US or between 1-12')
      }

      return r
    })
  }

  _parseDows (expr) {
    return this._parseSchedElem(expr, 'weekdays', 0, 6, a => {
      let r = parseInt(DateTime.fromFormat(a, 'EEEE').weekday ||
        DateTime.fromFormat(a, 'EEE').weekday || a, 10)
      if (Number.isNaN(r) || r < 0 || r > 7) {
        throw new Error('weekdays must be en-US or between 0-7')
      }
      if (r === 0) {
        r = 7
      }

      return r
    })
  }

  _parseSchedElem (expr, name, min, max, atoi) {
    if (expr === '*') {
      return null
    }
    try {
      const occurences = []
      const parts = expr.split(',')
      for (let part of parts) {
        const partOccurences = []
        let step = 1
        const m = /\/(\d+)$/.exec(part)
        if (m) {
          step = atoi(m[1])
          if (step <= 0) {
            throw new Error('step must be greater than 0')
          }
          if (step > max - min) {
            throw new Error('step too large')
          }
          part = part.replace(/\/\d+$/, '')
        }

        const span = part.split('-')
        if (span.length > 2) {
          throw new Error('too many hyphens')
        }
        if (span.length === 2) {
          const [start, stop] = span.map(a => atoi(a))
          if (start === stop) {
            // WESHOULD perhaps throw an exception here: did they mean '*' or
            // just `start`? But Vixie Cron understands this as just `start`.
            // But it would: it doesn't support high-low ranges. Perhaps there
            // should be a "compatible" flag that lets us throw here if it's
            // false, to help our users.
            partOccurences.push(start)
          } else if (start < stop) {
            this._pushRange(partOccurences, start, stop)
          } else {
            this._pushRange(partOccurences, min, stop)
            this._pushRange(partOccurences, start, max)
          }
        } else if (part === '*') {
          this._pushRange(partOccurences, min, max)
        } else {
          partOccurences.push(atoi(part))
        }

        for (let i = 0; i < partOccurences.length; i += step) {
          occurences.push(partOccurences[i])
        }
      }

      // sort and deduplicate occurences
      return occurences.sort((a, b) => a - b).filter((v, i) => i === 0 || occurences[i - 1] !== v)
    } catch (e) {
      e.message = `Bad ${name} expression '${expr}': ${e.message}`
      throw e
    }
  }

  _pushRange (arr, from, to) {
    for (let i = from; i <= to; i++) {
      arr.push(i)
    }
  }

  _handleDST (t, targetHour) {
    t = this._handleDSTStart(t, targetHour)
    t = this._handleDSTEnd(t)

    return t
  }

  _handleDSTStart (t, targetHour) {
    const targettingSkippedHour =
      targetHour === null ||
      targetHour === t.hour - 1
    const alsoTargettingFirstHourOfDst =
      targetHour === null ||
      this._targets[1] === null ||
      this._targets[1].includes(t.hour)

    if (this._isFirstHourOfDST(t) && targettingSkippedHour) {
      if (alsoTargettingFirstHourOfDst && this._opt.dstRunSkipped === 'always') {
        t[occurTwice] = true
      }
      if (!alsoTargettingFirstHourOfDst && !this._opt.dstRunSkipped) {
        t = null
      }
    }

    return t
  }

  _handleDSTEnd (t) {
    const repeating = this._isRepeatHourOfDST(t)
    const noRepeat = this._opt.dstNoRepeat
    if (repeating) {
      if (repeating === 'second' && noRepeat === 'first') {
        // This is the second time around, but we only want the first
        // occurence. Skip this one.
        return null
      }
      if (repeating === 'first' && noRepeat === 'second') {
        // This is the first time around, but we only want the second
        // occurence. Skip ahead one hour.
        t = t.plus({ hours: 1 })
      }
      if (noRepeat === 'auto' && repeating === 'second' &&
          this._targets[1] !== null && this._targets[1].length < 24
      ) {
        // This is the second time around, and this event doesn't repeat
        // 24h a day; let's skip it.
        return null
      }
    }

    return t
  }

  _isFirstHourOfDST (t) {
    return t !== null && t.isInDST && !t.minus({ hours: 1 }).isInDST
  }

  _isRepeatHourOfDST (t) {
    if (t === null) {
      return false
    }
    if (t.isInDST) {
      const t1 = t.plus({ hours: 1 })

      return (!t1.isInDST && t.hour === t1.hour) ? 'first' : false
    }

    const t1 = t.minus({ hours: 1 })

    return (t1.isInDST && t.hour === t1.hour) ? 'second' : false
  }
}

class Crontab {
  constructor (defaultOptions) {
    this._defaultOptions = defaultOptions
    this._plans = []
  }

  add (sched, ev, options) {
    this._plans.push(new Plan(sched, ev, options || this._defaultOptions))
  }

  at (t) {
    return this.between(t, t)
  }

  between (from, to) {
    return Array.from(this.genBetween(from, to))
  }

  * genBetween (from, to) {
    const [t0, t1, wantJSDates] = this._normalizeDates(from, to)

    const ret = []
    ret.addOccurence = (occurence) => {
      let from = 0
      let to = ret.length
      let i, cmp

      while (from <= to) {
        i = (to + from) >> 1
        cmp = ret[i] === undefined ? 0 : occurence.at - ret[i].at
        if (cmp > 0) {
          from = i + 1
        } else if (cmp < 0) {
          to = i - 1
        } else {
          from = i
          break
        }
      }

      ret.splice(from, 0, occurence)
    }

    ret.addNext = occurence => {
      const next = occurence.plan.next(occurence.at != null ? occurence.at.plus({ minutes: 1 }) : t0, t1)
      if (next === null) {
        return
      }
      for (let i = 0; i < (next[occurTwice] ? 2 : 1); i++) {
        ret.addOccurence({
          plan: occurence.plan,
          at: next
        })
      }
    }

    for (const plan of this._plans) {
      ret.addNext({ plan })
    }

    ret.shiftOccurence = () => {
      if (ret.length === 0) {
        return
      }
      const occurence = ret.shift()
      ret.addNext(occurence)

      return occurence
    }

    let occurence
    while ((occurence = ret.shiftOccurence(), occurence != null)) {
      yield {
        ev: occurence.plan.ev,
        at: wantJSDates ? occurence.at.toJSDate() : occurence.at
      }
    }
  }

  _normalizeDates (from, until) {
    let wantJSDates = false
    if (from instanceof Date && until instanceof Date) {
      from = DateTime.fromJSDate(from)
      until = DateTime.fromJSDate(until)
      wantJSDates = true
    }
    if (!(from instanceof DateTime && until instanceof DateTime)) {
      throw new Error(`Invalid dates ${from} and ${until}:` +
        ' must both be either Date or DateTime objects.')
    }
    if (from.zoneName !== until.zoneName) {
      throw new Error('from and until must be in the same time zone;' +
        ` have ${from.zoneName} and ${until.zoneName} respectively.`)
    }

    return [from, until, wantJSDates]
  }
}

module.exports.Plan = Plan
module.exports.Crontab = Crontab
